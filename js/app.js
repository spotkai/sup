var App = (function(window, $, Backbone, _) {
    "use strict";
    //todo ? add accordion to taskList by days
    //todo think up json-formatted ajax-msg
    var app = {},     //instance
        enterKey = 13,
        maxProjectNameLength = 12,
        projectThumbDetailsHeight = 562, //px
        maxAvailableDate = '9999-12-31';

    //
    var AppModel = Backbone.Model.extend({
        defaults: {
            currentModel: null,
            currentTask: null,
            currentState: 'work'
        },

        initialize: function() {
            this.newProjectAddedName = null;
            this.detailsOpened = false;
        }
    });
    var appModel = new AppModel();


    //
    var HeaderView = Backbone.View.extend({
        el: $('#header'),
        events: {
            'click #addProject': 'showAddProjectDialog',
            'click ul.dropdownMenu a': 'selectProjects',
            'click #reloadPage': 'reloadPage',
            'click .searchIcon': 'searchProject',
            'keyup .searchForm input': 'checkKeyup'
        },
        initialize: function() {
            this.listenTo(this.model, 'change:currentState', this.showCurrentState);
        },

        showAddProjectDialog: function(event) {
            app.showProjectDialog(event.target,
                $(templates['addProjectTemplate']()), 'Добавить проект',
                function() {
                    app.projectListView.addProject();
                });
        },

        selectProjects: function(ev) {
            var href = $(ev.target).attr('href'),
                state = href.slice(href.indexOf(':') + 1);
            if(appModel.get('currentState') !== state) {
                app.projectListView.loadProjects(state);
            }
        },

        showCurrentState: function() {
            appModel.detailsOpened = false;
            var stateRus;
            switch(appModel.get('currentState')) {
                case 'work':
                    stateRus = 'рабочие';
                    break;
                case 'query':
                    stateRus = 'запросы';
                    break;
                case 'arch':
                    stateRus = 'архив';
                    break;
                default:
                    stateRus = 'рабочие';
            }
            $('#selectProjects').find('span.statusSelected').html('~' + stateRus + '~');
        },

        reloadPage: function() {
            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        searchProject: function() {
            var searchNumber = $('.searchForm').find('input').val().toUpperCase(),
                searchModel = app.projectListView.collection.find(function(model) {
                    return model.get('projectName') === searchNumber;
                });
            if(searchModel) {
                var searchId = searchModel.get('id'),
                    $searchProject = $('#project_' + searchId);
                $('body, html').animate({'scrollTop': $searchProject.offset().top - 38 - $(window).height() / 2}, 400);
                $searchProject.click();
            }
        },

        checkKeyup: function(event) {
            event.preventDefault();
            var keycode = event.keyCode || event.which;
            if(keycode === enterKey) {
                this.searchProject();
            }
        }
    });
    app.headerView = new HeaderView({model: appModel});


    //
    var Project = Backbone.Model.extend({
        defaults: {
            projectName: null,
            projectState: null,
            projectParams: null,
            projectImg: null,
            projectTasks: null,
            projectData: null,
            projectDateStart: null,
            projectDateFinish: null
        },
        initialize: function() {
            this.on('error', function(model, xhr, options) {    //todo move to view
                Main.syncError(model, xhr, options);
            });
            this.on('request', function() {            //todo move to view
                Util.startLoadingAnimation();
            });
        },
        urlRoot: 'app/rest'
    });


    //
    var ProjectView = Backbone.View.extend({
        tagName: 'section',
        className: 'projectThumb',
        events: {
            'click': 'setCurrentModel',
            'click .showAddTask': 'showAddTaskDialog',
            'click .showEditProject': 'showEditProjectDialog',
            'click .showTaskHistory': 'toggleTaskHistory',
            'click .showDetails': 'showDetails'
        },
        initialize: function() {
            this.model.viewLink = this;
        },
        render: function() {
            this.$el.html(templates['projectThumbTemplate'](this.model.attributes))
                .attr('id', 'project_' + this.model.get('id'));
            if(appModel.get('currentState') === 'query' || appModel.get('currentState') === 'arch') {
                this.$el.addClass('arched');
            }
            return this;
        },

        setCurrentModel: function() {
            if(this.model !== appModel.get('currentModel')) {
                var $lastProject = $('#project_' + appModel.get('currentModel').get('id'));
                $('li.task').removeClass('selectedTask')
                    .find('.showEditTask').remove();
                //$lastProject.find('.showTaskHistory').click(); //fixme if(shows !!!)
                if(appModel.detailsOpened) {
                    $lastProject.find('.showDetails').click();
                }
                appModel.set('currentModel', this.model);
            }
        },

        showAddTaskDialog: function(event) {
            app.showTaskDialog(event.target,
                $(templates['addTaskTemplate']()),
                appModel.get('currentModel').get('projectName') + ': Добавить задачу',
                function() {
                    app.taskCalendarView.addTask();
                }
            );
        },

        showEditProjectDialog: function(ev) {
            var _this = this;
            Util.loadData('main/checkLock', {projectId: _this.model.get('id'), userId: Main.userModel.get('id')})
                .done(function(msg) {
                    if(typeof msg === 'object') {
                        //event.preventDefault();
                        //var projectName = appModel.get('currentModel').get('projectName');
                        //Main.router.navigate('#!project:' + projectName, {trigger: true});

                        _this.model.set(msg);
                        app.showProjectDialog(ev.target,
                            $(templates['addProjectTemplate'](_this.model.attributes)),
                            appModel.get('currentModel').get('projectName') + ': Редактировать проект',
                            function() {
                                app.projectListView.editProject();
                            });
                    }
                    else {
                        Main.appMsgView.showAppMsg('Проект уже редактируется пользователем <strong>'
                            + Main.userModel.get('allUsers')[msg] + '</strong>.');
                    }
                })
                .fail(function(msg) {
                    if(msg.responseText) {
                        Main.appMsgView.showAppMsg(msg.responseText);
                    }
                    else {
                        Main.appMsgView.showAppMsg('Ошибка при запросе данных');
                    }
                });
        },

        toggleTaskHistory: function(event) {
            $(event.currentTarget).toggleClass('showHistory');
            this.$('ul.taskBlock li.taskFinished').toggleClass('scene');
        },

        showDetails: function(event) {
            $(event.currentTarget).toggleClass('detOpened');
            this.$('.projectDetails').toggleClass('detOpened');

            var scrollTop = window.pageYOffset,
                windowHeight = $(window).height(),
                projectThumbHeight = this.$el.outerHeight(true);
            if(!appModel.detailsOpened) {
                projectThumbHeight += projectThumbDetailsHeight;
            }
            var bottomPosition = this.$el.offset().top + projectThumbHeight;
            if(bottomPosition > scrollTop + windowHeight) {
                $('body, html').animate({'scrollTop': '+' + (bottomPosition - windowHeight)}, 600);
            }

            appModel.detailsOpened = !!$(event.currentTarget).is('.detOpened');
        }
    });


    //
    var ProjectList = Backbone.Collection.extend({
        model: Project,
        url: 'app/rest'
    });


    //
    var ProjectListView = Backbone.View.extend({
        el: $('#projectList'),
        events: {
            'click li.task': 'markTask',
            'click span.showEditTask': 'editTask',
            'click .statePoint': 'showToggleStatePointTool',
            'click #toggleStatePointTool': 'toggleStatePoint'
        },

        initialize: function() {
            this.toggleStateLink = null;   //fixme
            this.collection = new ProjectList();
            this.listenTo(this.collection, 'reset', this.render);
            this.listenTo(this.collection, 'request', Util.startLoadingAnimation);
            this.listenTo(this.collection, 'error', function(model, xhr, options) {
                Main.syncError(model, xhr, options);
            });
            this.listenTo(this.model, 'change:currentModel', this.showSelectedProject);
            this.listenTo(this.model, 'change:currentTask', this.showMarkedTask);
            this.loadProjects(appModel.get('currentState'));
        },

        render: function() {
            Monitor.startMeasureTime();
            var _this = this;
            if(!this.sortModels()) {
                return this;
            }
            //render
            this.$('#projectListContent').html('');
            var states = {};
            states['state-design'] = document.createElement('div');
            states['state-production'] = document.createElement('div');
            states['state-shipping'] = document.createElement('div');
            states['state-design'].id = 'state-design';
            states['state-production'].id = 'state-production';
            states['state-shipping'].id = 'state-shipping';

            this.collection.forEach(function(item) {
                var projectView = new ProjectView({model: item}),
                    projState = _this.calcProjectState(item);
                states['state-' + projState].appendChild(projectView.render().el);
            });

            var fragment = document.createDocumentFragment();
            fragment.appendChild(states['state-design']);
            fragment.appendChild(states['state-production']);
            fragment.appendChild(states['state-shipping']);
            this.$('#projectListContent').append(fragment);
            states = null;

            this.showSelectedProject();
            Monitor.stopMeasureTime();
            return this;
        },

        //
        sortModels: function() {
            if(this.collection.models.length < 1) {  //empty
                appModel.set({'currentModel': null}, {silent: true});
                return false;
            }

            if(appModel.newProjectAddedName) {   //new added project set as current
                var lastModel = _.last(this.collection.models);
                if(appModel.newProjectAddedName === lastModel.get('projectName')) {
                    appModel.set({'currentModel': lastModel}, {silent: true});
                }
                appModel.newProjectAddedName = null;  //clear flag
            }

            this.collection.models = this.collection.sortBy(function(item) {
                if(appModel.get('currentState') === 'query') {
                    return -new Date(item.get('projectDateStart')).getTime();
                }
                else if(appModel.get('currentState') === 'arch') {
                    return -new Date(item.get('projectDateFinish')).getTime();
                }
                else {
                    return item.get('projectDateFinish') || maxAvailableDate;
                }
            });

            //if no currentModel(@init || no new project added) || previous currentModel has switched to other set<>
            if(!appModel.get('currentModel') || !this.collection.get(appModel.get('currentModel').get('id'))) {
                appModel.set({'currentModel': this.collection.models[0]}, {silent: true});  //set first as current
            }
            else {            //set as previous
                appModel.set({'currentModel': this.collection.get(appModel.get('currentModel').get('id'))}, {silent: true});
            }
            return true;
        },

        //
        loadProjects: function(projState) {
            var _this = this;
            this.collection.fetch({
                data: $.param({state: projState}),
                success: function(collection, response, options) {
                    appModel.set('currentState', projState);
                    _this.collection.reset(response);
                    Util.stopLoadingAnimation();
                }
            });
        },

        //
        showSelectedProject: function() {
            var $project = $('#project_' + appModel.get('currentModel').get('id'));
            this.$('.selected, .expanded').removeClass('selected expanded');
            $project.addClass('selected');
            if(appModel.detailsOpened) {   //show details opened
                $project.find('.showDetails').click();
            }
            if(appModel.get('currentState') === 'query' || appModel.get('currentState') === 'arch') {
                $project.toggleClass('expanded');
            }
        },

        //
        addProject: function() {
            var _this = this,
                data = this.getFormData('addProject'),
                names = this.collection.pluck('projectName');
            if(data.projectName) {
                if(!_.contains(names, data.projectName)) {
                    (new Project(data)).save({}, {
                        success: function() {
                            Util.stopLoadingAnimation();
                            appModel.newProjectAddedName = data.projectName;
                            _this.loadProjects(appModel.get('currentState'));
                        }
                    });
                    Main.dialogView.dialogDestroy();
                }
                else {
                    $('#dialogMsg').html('Такой проект уже существует');
                }
            }
            else {
                $('#dialogMsg').html('Укажите название проекта');
            }
        },

        //
        editProject: function() {
            var _this = this,
                data = this.getFormData('editProject'),
                names = _.without(this.collection.pluck('projectName'), appModel.get('currentModel').get('projectName')),
                modelIsChanged = false;
            for(var attr in data) {
                if(data.hasOwnProperty(attr) && !_.isEqual(data[attr], appModel.get('currentModel').get(attr))) {
                    modelIsChanged = true;
                    break;
                }
            }
            if(data.projectName) {
                if(!_.contains(names, data.projectName)) {
                    if(modelIsChanged) {
                        appModel.get('currentModel').save(data, {
                            patch: true,
                            success: function() {
                                Util.stopLoadingAnimation();
                                _this.loadProjects(appModel.get('currentState'));
                            }
                        });
                        Main.dialogView.dialogDestroy();
                    }
                    else {
                        $('#dialogMsg').html('Ничего не изменено');
                    }
                }
                else {
                    $('#dialogMsg').html('Такой проект уже существует');
                }
            }
            else {
                $('#dialogMsg').html('Укажите название проекта');
            }
        },

        //
        getFormData: function(state) {
            var formData = {},
                data = {};
            $('#formAddProject').find('input, textarea').each(function() {
                var value = null;
                if($(this).attr('name') === 'projectImg') {
                    value = $(this).siblings('span.loadedImg')
                        .css('background-image').replace('url(', '').replace(')', '').replace(/^"(.*)"$/, '$1');
                }
                else if($(this).attr('name') === 'projectStateZ') {
                    value = 'work';
                    if($(this).is(':checked')) {
                        value = 'query';
                    }
                    else if(state === 'editProject' && appModel.get('currentModel').get('projectState') === 'arch') {
                        value = 'arch';
                    }
                }
                else if($(this).attr('name') === 'projectName') {
                    value = $(this).val().toUpperCase().slice(0, maxProjectNameLength) || null;
                }
                else {
                    value = $(this).val() || null;
                }
                formData[$(this).attr('name')] = value;
            });

            data.projectImg = formData.projectImg;
            data.projectState = formData.projectStateZ;
            data.projectName = formData.projectName;
            data.projectDateFinish = formData.projectDateFinish;
            data.projectParams = [
                {title: 'код изделия', value: formData['paramSubjectCode']},
                {title: 'цвет металла', value: formData['paramColorMe']},
                {title: 'текстура ДСП', value: formData['paramTextureDSP']},
                {title: 'цвет кромки', value: formData['paramEdgeDSP']},
                {title: 'заметка', value: formData['paramNote']}
            ];
            return data;
        },

        //
        markTask: function(event) {
            var currentTaskIndex = $(event.currentTarget).attr('data-id');
            appModel.set('currentTask', {
                'currentModelId': appModel.get('currentModel').get('id'),
                'currentTaskIndex': currentTaskIndex
            });
            appModel.trigger('change:currentTask');
        },

        //
        showMarkedTask: function() {
            if(appModel.get('currentTask')) {
                this.$('li.task').removeClass('selectedTask')
                    .find('.showEditTask').remove();
                $('#project_' + appModel.get('currentTask').currentModelId)
                    .find('li.task[data-id=' + appModel.get('currentTask').currentTaskIndex + ']').addClass('selectedTask')
                    .append('<span class="showEditTask"></span>');
            }
        },

        //
        editTask: function(event) {
            app.taskCalendarView.changeTask(event);
        },

        //
        showToggleStatePointTool: function(ev) {
            if(this.$('.picker').length) {
                return;
            }
            var clickX = $(ev.currentTarget).position().left,
                clickY = $(ev.currentTarget).position().top,
                posX = 0, posY = 0;
            this.toggleStateLink = $(ev.currentTarget);

            if($(ev.target).is('.statePointDate.datepickerLink')) {
                var $pickadateContainer = $('#pickadateContainer');
                posX = clickX + 46;
                posY = clickY + $(ev.currentTarget).outerHeight(true);
                $pickadateContainer.css({top: posY, left: posX}).addClass('scene');
                $(ev.target).pickadate({
                    container: $pickadateContainer,
                    onClose: function() {
                        $pickadateContainer.removeClass('scene');
                        app.projectListView.toggleStatePoint(ev);
                        this.stop();
                    }
                });
                return;
            }

            $('.statePoint').removeClass('selectedRow');
            var $toggleStatePointTool = $('#toggleStatePointTool');
            posX = clickX + 20;
            posY = clickY - $toggleStatePointTool.outerHeight(true);
            $toggleStatePointTool.css({top: posY, left: posX}).addClass('showTool');
            $(ev.currentTarget).addClass('selectedRow');
        },

        //
        toggleStatePoint: function(event) {
            var $currentTarget = this.toggleStateLink,
                pointName = $currentTarget.attr('data-id'),
                groupName = $currentTarget.closest('li.stateGroup').attr('data-id'),
                data = appModel.get('currentModel').get('projectData'),
                point = data[groupName][pointName];

            if($(event.target).is('.statePointDate.datepickerLink') && point.date !== $(event.target).val()) {
                var dateChanged = true;
            }
            else {
                var action = $(event.target).attr('data-id');
            }

            if(dateChanged || (action && point.status !== action)) {
                if(action) {
                    point.status = action;
                    if('date' in point) {
                        if(point.status === 'finished') {
                            point.date = Util.getDate();
                        }
                        else {
                            point.date = null;
                        }
                    }
                }
                else if('date' in point) {
                    point.date = $(event.target).val();
                }

                appModel.get('currentModel').save({projectData: data}, {
                    patch: true,
                    success: function() {
                        Util.stopLoadingAnimation();
                        app.projectListView.loadProjects(appModel.get('currentState'));
                    }
                });
            }
            $('#toggleStatePointTool').removeClass('showTool');
            $currentTarget.removeClass('selectedRow');
            this.toggleStateLink = null;
        },

        //
        calcProjectState: function(item) {
            var projData = item.get('projectData');
            for(var pState = 0, pStateLength = projData.length; pState < pStateLength; pState++) {
                for(var point = 1, pointLength = projData[pState].length; point < pointLength; point++) {
                    if(projData[pState][point].status === 'inwork') {
                        switch(projData[pState][0].groupName) {
                            case 'Проектирование':
                                return 'design';
                            case 'Производство':
                                return 'production';
                            case 'Отгрузка':
                                return 'shipping';
                            default:
                                return 'design';
                        }

                    }
                }
            }
            return 'shipping';
        },

        //
        setProjectState: function(state) {
            var _this = this;
            appModel.get('currentModel').save({projectState: state}, {
                patch: true,
                success: function() {
                    Util.stopLoadingAnimation();
                    appModel.detailsOpened = false;
                    _this.loadProjects(appModel.get('currentState'));
                }
            });
            Main.dialogView.dialogDestroy();
        }
    });
    app.projectListView = new ProjectListView({model: appModel});


    //
    var TaskCalendarView = Backbone.View.extend({
        el: $('#taskCalendar'),
        events: {
            'click li.task, li.userTask': 'selectTask',
            'click span.showEditTask': 'changeTask'
        },

        initialize: function() {
            this.tasks = [];
            this.listenTo(this.model, 'change:currentTask', this.showSelectedTask);
            this.listenTo(app.projectListView.collection, 'reset', this.render);
        },

        render: function() {
            this.generateTaskList();
            $('#taskCalendarContent').html(templates['taskListTemplate']({
                taskList: this.tasks,
                userTasks: Main.userModel.get('userTasks')
            }));
            return this;
        },

        selectTask: function(ev) {
            var $currentTask = $(ev.currentTarget).attr('data-id'),
                currentModelId = $currentTask.slice(0, $currentTask.indexOf('_')),
                currentTaskIndex = $currentTask.slice($currentTask.indexOf('_') + 1) || null;
            if(currentModelId !== 'user') {
                $('#project_' + currentModelId).click();
            }
            appModel.set('currentTask', {'currentModelId': currentModelId, 'currentTaskIndex': currentTaskIndex});
            appModel.trigger('change:currentTask');
        },

        showSelectedTask: function() {
            if(appModel.get('currentTask')) {
                var dataId = appModel.get('currentTask').currentModelId + '_' + appModel.get('currentTask').currentTaskIndex;
                this.$('li.task, li.userTask').removeClass('selectedTask')
                    .find('.showEditTask').remove();
                this.$('li.task[data-id=' + dataId + '], li.userTask[data-id=' + dataId + ']').addClass('selectedTask')
                    .append('<span class="showEditTask"></span>');
            }
        },

        //
        addTask: function(status) {      //fixme add check something changed
            var formData = {};
            $('#formAddTask').find('textarea, input, select').each(function() {
                formData[$(this).attr('name')] = $(this).val() || null;
            });
            if(formData['taskText']) {
                var tasks = appModel.get('currentModel').get('projectTasks');
                tasks.push({
                    taskText: formData['taskText'], taskDate: formData['taskDate'],
                    taskPerformer: formData['taskPerformer'], taskStatus: status || 'opened'
                });
                tasks = _.sortBy(tasks, function(item) {
                    return item.taskDate;
                });
                this.saveTasks(tasks);
            }
            else {
                $('#dialogMsg').html('Введите текст задачи');
            }
        },

        //
        changeTask: function(ev) {
            var _this = this,
                $taskElement = $(ev.target).closest('li');
            if($taskElement.is('.userTask')) {
                Main.userView.changeUserTask($taskElement);
                return;
            }
            Util.loadData('main/checkLock', {
                projectId: appModel.get('currentModel').get('id'),
                userId: Main.userModel.get('id')
            })
                .done(function(msg) {
                    if(typeof msg === 'object') {
                        appModel.get('currentModel').set(msg);
                        var tasks = appModel.get('currentModel').get('projectTasks'),
                            selectedTaskIndex = appModel.get('currentTask').currentTaskIndex;
                        if(tasks[selectedTaskIndex]) {
                            app.showTaskDialog($taskElement,
                                $(templates['addTaskTemplate'](tasks[selectedTaskIndex])),
                                appModel.get('currentModel').get('projectName') + ': Редактировать задачу',
                                function() {
                                    tasks.splice(selectedTaskIndex, 1);
                                    _this.addTask();
                                });
                        }
                        else {
                            Main.appMsgView.showAppMsg('Задача была удалена другим пользователем.<br/>Обновите проекты.');
                        }
                    }
                    else {
                        Main.appMsgView.showAppMsg('Проект уже редактируется пользователем <strong>'
                            + Main.userModel.get('allUsers')[msg] + '</strong>.');
                    }
                })
                .fail(function(msg) {
                    if(msg.responseText) {
                        Main.appMsgView.showAppMsg(msg.responseText);
                    }
                    else {
                        Main.appMsgView.showAppMsg('Ошибка при запросе данных');
                    }
                });
        },

        //
        deleteTask: function() {
            var selectedTaskIndex = appModel.get('currentTask').currentTaskIndex;
            if(appModel.get('currentModel') && selectedTaskIndex) {
                var tasks = appModel.get('currentModel').get('projectTasks');
                tasks.splice(selectedTaskIndex, 1);
                this.saveTasks(tasks);
            }
        },

        //
        changeTaskStatus: function(status) {
            var selectedTaskIndex = appModel.get('currentTask').currentTaskIndex;
            if(appModel.get('currentModel') && selectedTaskIndex) {
                var tasks = appModel.get('currentModel').get('projectTasks');
                if(status === 'finished') {
                    tasks.splice(selectedTaskIndex, 1);
                    this.addTask(status);
                }
                else {
                    tasks[selectedTaskIndex].taskStatus = status;
                    this.saveTasks(tasks);
                }
            }
        },

        //
        saveTasks: function(tasks) {
            appModel.get('currentModel').save({projectTasks: tasks}, {
                patch: true,
                success: function() {
                    Util.stopLoadingAnimation();
                    appModel.set('currentTask', null);
                    app.projectListView.loadProjects(appModel.get('currentState'));
                }
            });
            Main.dialogView.dialogDestroy();
        },

        //
        generateTaskList: function() {
            var j = 0,
                projTasks = null,
                taskList = [];
            app.projectListView.collection.forEach(function(item) {
                projTasks = item.get('projectTasks');
                projTasks.forEach(function(task, taskIndex) {
                    if(task.taskStatus === 'opened') {
                        taskList[j] = {};
                        taskList[j].order = taskIndex.toString();
                        taskList[j].taskText = task.taskText;          //fixme no need?? use modelLink.get('projectTasks')[currentOrder].taskText
                        taskList[j].taskDate = task.taskDate || '...'; //fixme no need?? use modelLink.get('projectTasks')[currentOrder].taskDate
                        taskList[j].taskPerformer = task.taskPerformer;
                        taskList[j].taskStatus = task.taskStatus;
                        taskList[j].modelLink = item;
                        j++;
                    }
                });
            });

            taskList = _.sortBy(taskList, function(item) {
                if(item.taskDate === '...') {
                    return maxAvailableDate;
                }
                else {
                    return item.taskDate;
                }
            });
            taskList = _.groupBy(taskList, function(item) {
                return item.taskDate;
            });
            this.tasks = taskList;
        }
    });
    app.taskCalendarView = new TaskCalendarView({model: appModel});


    //prod events
    function setEventHandlers() {

        //
        $('body').on('click', function(e) {   //fixme !!!!!!!!!!!!!!!!!!!!!!!
            if(!$(e.target).closest('div, ul').is('.toolElement, .statePointBlock') || $(e.target).is('.statePointDate')) {
                $('.toolElement').removeClass('showTool');
                $('.statePoint').removeClass('selectedRow');
            }
        });

        //
        $(window).unload(function() {
            $.ajaxSetup({async: false});
            Main.dialogView.dialogDestroy();
            Monitor.logExecutionTime('app.js');
        });
    }


    //
    app.showProjectDialog = function(ev, $content, title, callback) {
        var butt = {
            'Сохранить': callback
        };
        if(title.indexOf('Редактировать проект') !== -1) {
            var projState = appModel.get('currentModel').get('projectState');
            if(projState === 'work') {
                butt['Завершить проект'] = function() {
                    app.projectListView.setProjectState('arch');
                }
            }
            else if(projState === 'arch') {
                butt['Открыть проект'] = function() {
                    app.projectListView.setProjectState('work');
                }
            }
        }
        Main.dialogView.makeDialog({
            ev: ev,
            $content: $content,
            title: title,
            buttons: butt,
            beforeClose: function() {
                if(title.indexOf('Редактировать проект') !== -1) {
                    app.freeProject();
                }
            }
        });
    };


    //
    app.showTaskDialog = function(ev, $content, title, callback) {
        var butt = {
            'Сохранить': callback
        };
        if(title.indexOf('Редактировать задачу') !== -1) {
            butt['Удалить задачу'] = function() {
                app.taskCalendarView.deleteTask();
            }
        }
        if($(ev).is('li.task')) {  //not new task
            if(ev.hasClass('taskFinished')) {
                butt['Открыть задачу'] = function() {
                    app.taskCalendarView.changeTaskStatus('opened');
                };
            }
            else {
                butt['Завершить задачу'] = function() {
                    app.taskCalendarView.changeTaskStatus('finished');
                };
            }
        }
        Main.dialogView.makeDialog({
            ev: ev,
            $content: $content,
            title: title,
            buttons: butt,
            beforeClose: function() {
                if(title.indexOf('Редактировать задачу') !== -1) {
                    app.freeProject();
                }
            }
        });
    };


    //
    app.freeProject = function() {
        Util.loadData('main/freeLock', {projectId: appModel.get('currentModel').get('id')});
    };


    //
    setEventHandlers();
    return app;
})(window, jQuery, Backbone, _);
//@ sourceURL=app.js