var Monitor = (function(window, $, _) {
    "use strict";

    var monitor = {},        //instance
        monitorData = {execTimes: []};   //value stack
    //fixme add loadPageTime, parsePageTime

    //
    monitor.startMeasureTime = function() {
        monitorData.timeStart = new Date().getTime();
    };


    //
    monitor.stopMeasureTime = function() {
        var executionTime = new Date().getTime() - monitorData.timeStart;
        monitorData.timeStart = null;
        monitorData.execTimes.push(executionTime);
        console.log(executionTime);  //todo remove
    };


    //$(window).unload
    monitor.logExecutionTime = function(resource) {
        var minTime = _.min(monitorData.execTimes),
            maxTime = _.max(monitorData.execTimes),
            averageTime = _.reduce(monitorData.execTimes, function(memo, num) {
                    return memo + num;
                }, 0) / (monitorData.execTimes.length === 0 ? 1 : monitorData.execTimes.length);
        monitorData.execTimes = [];

        Util.loadData('monitor/logExecutionTime', {
            resource: resource,
            executionTime: {averageTime: averageTime, minTime: minTime, maxTime: maxTime}
        });
    };


    return monitor;
})(window, jQuery, _);
//@ sourceURL=monitor.js