//event mediator
//Main.mediator = _.extend({
//    function(obj){
//
//        var channels = {};
//        if(!obj) obj = {};
//
//        obj.subscribe = function(channel, subscription) {
//            if(!channels[channel]) channels[channel] = [];
//            channels[channel].push(subscription);
//        };
//
//        obj.publish = function(channel) {
//            if(!channels[channel]) return;
//            var args = [].slice.call(arguments, 1);
//            for(var i = 0, l = channels[channel].length; i < l; i++) {
//                channels[channel][i].apply(this, args);
//            }
//        };
//
//        return obj;
//
//   }
//},Backbone.Events);


var mediator = (function() {
    var subscribe = function(channel, fn) {
            if(!mediator.channels[channel]) mediator.channels[channel] = [];
            mediator.channels[channel].push({context: this, callback: fn});
            return this;
        },

        publish = function(channel) {
            if(!mediator.channels[channel]) return false;
            var args = Array.prototype.slice.call(arguments, 1);
            for(var i = 0, l = mediator.channels[channel].length; i < l; i++) {
                var subscription = mediator.channels[channel][i];
                subscription.callback.apply(subscription.context, args);
            }
            return this;
        };

    return {
        channels: {},
        publish: publish,
        subscribe: subscribe,
        installTo: function(obj) {
            obj.subscribe = subscribe;
            obj.publish = publish;
        }
    };

}());

//Example: Here are two sample uses of the implementation from above.

//Pub/sub on a centralized mediator
mediator.name = "tim";
mediator.subscribe('nameChange', function(arg) {
    console.log(this.name);
    this.name = arg;
    console.log(this.name);
});
mediator.publish('nameChange', 'david'); //tim, david


//Pub/sub via third party mediator
var obj = {name: 'sam'};
mediator.installTo(obj);
obj.subscribe('nameChange', function(arg) {
    console.log(this.name);
    this.name = arg;
    console.log(this.name);
});
obj.publish('nameChange', 'john'); //sam, john