var User = (function(window, $, Backbone, _) {
    "use strict";

    var enterKey = 13,
        tabKey = 9;

    //
    var LoginView = Backbone.View.extend({
        el: $('#loginWrap'),
        events: {
            'blur #loginName': 'checkUserName',
            'click button[name=enterLogin]': 'loginUser',
            'keyup #formLogin input': 'checkKeyup',
            'focus #formLogin input': function() {
                $('#loginMsg').empty();
            },
            'focus #formReg input': function() {
                $('#regMsg').empty();
            },
            'click #openReg': 'openRegistration',
            'change #userRegImg': function(event) {
                Util.loadImg(event, 36);
            },
            'click button[name=enterReg]': 'registerUser'
        },

        initialize: function() {
            this.$('input:first').focus();
        },

        checkUserName: function() {
            var loginName = $('#loginName').val();
            if(loginName.length) {
                var formData = {loginName: loginName};
                Util.loadData('user/checkUserName', formData)
                    .done(function(msg) {
                        if(msg === 'ERROR') {
                            $('#loginName').find('+span').css('backgroundImage', 'url()');
                            showLoginErrorMsg('Неверный логин');
                        }
                        else {
                            $('#loginName').find('+span').css('backgroundImage', 'url(' + msg + ')');
                        }
                    })
                    .fail(function(msg) {
                        showLoginErrorMsg(msg.responseText);
                    });
            }
        },

        loginUser: function() {
            if($('#loginMsg').html().length === 0) {
                var loginName = $('#loginName').val(),
                    loginPassword = $('#loginPassword').val();
                if(loginName && loginPassword) {
                    var formData = {loginName: loginName, loginPassword: loginPassword};
                    Util.loadData('user/loginUser', formData)
                        .done(function(msg) {
                            showLoginErrorMsg(msg);
                        })
                        .fail(function(msg) {
                            showLoginErrorMsg(msg.responseText);
                        });
                }
            }
        },

        checkKeyup: function(event) {
            var keycode = event.keyCode || event.which,
                $loginMsg = $('#loginMsg');
            if(keycode === enterKey) {
                this.loginUser();
            }
            else if(keycode !== tabKey && $loginMsg.html().length > 0) {
                $loginMsg.empty();
            }
        },

        openRegistration: function(event) {
            event.preventDefault();
            $('#formRegWrap').toggleClass('regOpened');
        },

        registerUser: function() {
            if($('#regMsg').html().length === 0) {
                var formData = {
                    regName: $('#regName').val(),
                    regFirstname: $('#regFirstname').val(),
                    regLastname: $('#regLastname').val(),
                    regPassword: $('#regPassword').val(),
                    regPassword2: $('#regPassword2').val(),
                    userRegImg: $('#userRegImg').siblings('span.loadedImg').css('background-image')
                        .replace('url(', '').replace(')', '').replace(/^"(.*)"$/, '$1'),
                    regKey: $('#regKey').val()
                };
                if(formData['regName']) {
                    if(formData['regFirstname'] && formData['regLastname']) {
                        if(formData['regPassword'] && formData['regPassword2']) {
                            if(formData['regKey']) {
                                Util.loadData('user/registerUser', formData)
                                    .done(function(msg) {
                                        showRegErrorMsg(msg);
                                    })
                                    .fail(function(msg) {
                                        showRegErrorMsg(msg.responseText);
                                    });
                            }
                            else {
                                showRegErrorMsg('укажите ключ');
                            }
                        }
                        else {
                            showRegErrorMsg('укажите пароль');
                        }
                    }
                    else {
                        showRegErrorMsg('укажите имя/фамилию');
                    }
                }
                else {
                    showRegErrorMsg('укажите логин');
                }
            }
        }

    });
    new LoginView();


    //
    function showLoginErrorMsg(msg) {
        $('#loginMsg').html(msg);
    }


    //
    function showRegErrorMsg(msg) {
        $('#regMsg').html(msg);
    }


})(window, jQuery, Backbone, _);
