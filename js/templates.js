var temp = {};

temp.projectThumbTemplate =
    '<div class="thumbContent">\
       <div class="projectThumbTools<% if(projectState === \"arch\") { %> arched<% } %>">\
         <% if(projectState !== "arch") { %>\
           <span class="showAddTask"></span>\
         <% } %>\
           <span class="showEditProject"></span>\
           <span class="showTaskHistory"></span>\
           <span class="showDetails"></span>\
       </div>\
       <span class="projImg" style="background-image: url(<%= projectImg %>)"></span>\
       <div>\
           <h3><%= projectName %></h3>\
           <h6><%= projectClient %></h6>\
           <strong class="projDateFinish<%if(projectState !== \"arch\" && projectDateFinish && new Date().getTime() > (new Date(projectDateFinish).getTime()+15*60*60*1000)){%> expired<%}%>\"><%= Util.prettifyDate(projectDateFinish, true) %></strong>\
           <ul class="paramBlock">\
               <% projectParams.forEach(function(param, paramIndex) { %>\
               <li data-id="<%= paramIndex %>"><span><%= param.title %>: </span><%= param.value %></li>\
               <% }); %>\
           </ul>\
           <ul class="taskBlock">\
               <% projectTasks.forEach(function(task, taskIndex) { %>\
                 <li class="task<% if(task.taskStatus === \"finished\") {%> taskFinished<%}%>" data-id="<%= taskIndex %>"><strong<%if(task.taskDate && new Date().getTime() > (new Date(task.taskDate).getTime()+15*60*60*1000)){%> class=\"expired\"<%}%>><%= Util.prettifyDate(task.taskDate) %></strong><br/><%= task.taskText %><br/><strong>Исполнитель: <%= Main.userModel.get(\"allUsers\")[task.taskPerformer] %></strong></li>\
              <% }); %>\
           </ul>\
       </div>\
       <span class="projectStatus status-<% if(projectData[0][1].status === \"inwork\" || projectData[0][2].status === \"inwork\") { %>queryWork<% } else { %><%= projectState %><% } %>"></span>\
   </div>\
   <div class="projectDetails">\
       <ul class="stateGroupBlock">\
           <% projectData.forEach(function(projState, stateIndex) { %>\
           <li class="stateGroup" data-id="<%= stateIndex %>">\
               <span><%= projState[0].groupName %></span>\
               <ul class="statePointBlock">\
                   <%  projState.forEach(function(point, pointIndex) {\
                   if(pointIndex !== 0) { %>\
                       <li class="statePoint <%= point.status %>" data-id="<%= pointIndex %>"><span><%= point.title %></span>\
                       <% if(typeof point.date !== "undefined" && point.date) { %>\
                         <input type="text" class="statePointDate datepickerLink" name="statePointDate" value="<%= point.date %>"/>\
                       <% } %>\
                       </li>\
                   <% }}); %>\
               </ul>\
           </li>\
           <% }); %>\
       </ul>\
   </div>';


temp.userTemplate =
    '<div id="userName" class="dropdown">\
       <span class="userImg" style="background-image: url(<%= userAvatar %>)"></span>\
       <a href="#userTab"><%= userName %></a>\
       <ul class="dropdownMenu">\
           <li><a href="#!user:addUserTask">Добавить&nbsp;задачу</a></li>\
           <li><a href="#!user:userSettings">Настройки</a></li>\
           <li><a href="#!user:logout">Выйти</a></li>\
       </ul>\
     </div>';


temp.taskListTemplate =
    '<ul id="taskList">\
       <% for(var taskDate in taskList) { %>\
           <li <%var today = Util.getDate();\
                 if(taskDate === today){%> class="today"<%}\
                 else if(taskDate < today && taskDate !== "..."){%> class="expired"<%}%>>\
               <span class="taskDate<%if(new Date().getTime() > new Date(taskDate).getTime()+15*60*60*1000){%> expired<%}%>\"><%= Util.prettifyDate(taskDate) %>,&nbsp;<%= Util.getDay(taskDate) %></span>\
               <ul class="taskBlockList">\
               <% taskList[taskDate].forEach(function(task, taskIndex) { %>\
                   <li class="task" data-id="<%= task.modelLink.get(\"id\") %>_<%= task.order %>"><span class="projectLinkClient"><%= task.modelLink.get("projectClient") %></span><span class="projectLink"> &#91;<%= task.modelLink.get("projectName") %>&#93;</span><br/><%= task.taskText %><br/><strong>Исполнитель: <%= Main.userModel.get(\"allUsers\")[task.taskPerformer] %></strong></li>\
               <% }); %>\
               </ul>\
           </li>\
       <% } %>\
           <li class="userTaskBlockList"><span class="taskDate">&#126;<%= Main.userModel.get("userName") %>&#126;</span>\
               <ul class="taskBlockList">\
               <% userTasks.forEach(function(userTask, userTaskIndex) { %>\
                   <li class="userTask" data-id="user_<%= userTaskIndex %>"><%= userTask %></li>\
               <% }); %>\
               </ul>\
           </li>\
       </ul>';


temp.userDataTemplate =
    '<form id="userData" method="POST" action="#" autocomplete="off">\
        <div class="formItem">\
            <label for="oldPassword">Введите текущий пароль</label>\
            <input type="password" id="oldPassword" name="oldPassword" required/>\
        </div>\
        <h3>Смена пароля</h3>\
        <div class="formItem">\
            <label for="inputPassword">Новый пароль</label>\
            <input type="password" id="inputPassword" name="inputPassword" required/>\
        </div>\
        <h3>Смена аватара</h3>\
        <div class="formItem">\
            <label for="userDataImg">Аватар</label>\
            <span class="userImg loadedImg" style="background-image: url(<%= userAvatar %>)"></span>\
            <input id="userDataImg" type="file" name="userDataImg"/>\
        </div>\
     </form>';


temp.addTaskTemplate =
    '<div id="addTaskDialog">\
        <form id="formAddTask" action="#" autocomplete="off" title="Добавление задачи">\
            <div class="formItem">\
                <textarea rows="9" cols="49" name="taskText" required<% if(typeof taskStatus !== "undefined" && taskStatus === \"finished\") {%> disabled<%}%>><% if(typeof taskText !== "undefined") { %><%= taskText %><% } %></textarea>\
            </div>\
            <div class="formItem">\
                <label for="taskDate">Дата выполнения</label>\
                <input type="text" class="datepickerLink" name="taskDate" required\
                  <% if(typeof taskDate !== "undefined"){%>value="<%= taskDate %>"<%}%><% if(typeof taskStatus !== "undefined" && taskStatus === \"finished\") {%> disabled<%}%>/>\
            </div>\
            <div class="formItem">\
                <label for="taskPerformer">Исполнитель</label>\
                <select name="taskPerformer" required<% if(typeof taskStatus !== "undefined" && taskStatus === \"finished\") {%> disabled<%}%>>\
                    <% _.each(Main.userModel.get(\"allUsers\"), function(user, userIndex) { %>\
                    <option value="<%= userIndex %>" <% if(typeof taskPerformer !== "undefined" && taskPerformer === userIndex) { %> selected<% } %>><%= user %></option>\
                    <% }); %>\
                </select>\
            </div>\
        </form>\
     </div>';


temp.addUserTaskTemplate =
    '<div id="addTaskDialog">\
        <form id="formAddTask" action="#" autocomplete="off" title="Добавление задачи">\
            <div class="formItem">\
                <textarea rows="9" cols="49" name="taskText" required><% if(typeof taskText !== "undefined") { %><%= taskText %><% } %></textarea>\
            </div>\
        </form>\
     </div>';


temp.addProjectTemplate =
    '<div id="addProjectDialog">\
        <form id="formAddProject" action="#" autocomplete="off" title="Добавление проекта">\
            <div class="formItem">\
                <label for="projectImg">Эскиз</label>\
                <span class="projImg loadedImg"\
                    style="background-image: url(<% if(typeof projectImg !== \"undefined\") { %><%= projectImg %><% }\
                          else { %>data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==<% } %>)">\
                </span>\
                <input id="projectImg" type="file" name="projectImg"/>\
            </div>\
            <div class="formItem">\
                <label for="projectStateZ">Запрос</label>\
                <input type="checkbox" name="projectStateZ"\
                  <% if(typeof projectState !== "undefined" && projectState === "query") { %>checked<% } %>/>\
            </div>\
            <div class="formItem">\
                <label for="projectName">Номер проекта</label>\
                <input type="text" name="projectName"\
                  <% if(typeof projectName !== "undefined") { %>value="<%= projectName %>"<% } %>required/>\
            </div>\
            <div class="formItem">\
                <label for="projectDateFinish">Срок поставки</label>\
                <input type="text" class="datepickerLink" name="projectDateFinish"\
                  <% if(typeof projectDateFinish !== "undefined") { %>value="<%= projectDateFinish %>"<% } %>required/>\
            </div>\
            <div class="formItem">\
                <label for="paramSubjectCode">Код изделия</label>\
                <input type="text" name="paramSubjectCode"\
                  <% if(typeof projectParams !== "undefined") { %>value="<%= projectParams[0].value %>"<% } %>/>\
            </div>\
            <div class="formItem">\
                <label for="paramColorMe">Цвет металла</label>\
                <input type="text" name="paramColorMe"\
                  <% if(typeof projectParams !== "undefined") { %>value="<%= projectParams[1].value %>"<% } %>/>\
            </div>\
            <div class="formItem">\
                <label for="paramTextureDSP">Текстура ДСП</label>\
                <input type="text" name="paramTextureDSP"\
                  <% if(typeof projectParams !== "undefined") { %>value="<%= projectParams[2].value %>"<% } %>/>\
            </div>\
            <div class="formItem">\
                <label for="paramEdgeDSP">Цвет кромки</label>\
                <input type="text" name="paramEdgeDSP"\
                  <% if(typeof projectParams !== "undefined") { %>value="<%= projectParams[3].value %>"<% } %>/>\
            </div>\
            <div class="formItem">\
                <label for="paramNote">Заметка</label>\
                <textarea rows="6" cols="43" name="paramNote"><% if(typeof projectParams !== "undefined") { %><%= projectParams[4].value %><% } %></textarea>\
            </div>\
        </form>\
     </div>';


var templates = {};
Object.keys(temp).forEach(function(item) {
    templates[item] = _.template(temp[item]);
});
temp = null;
