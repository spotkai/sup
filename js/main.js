var Main = (function(window, $, Backbone, _) {
    "use strict";

    var main = {};      //instance

    //
    Backbone.View.prototype.closeView = function() {
        this.stopListening();
        this.off();
        this.remove();
    };


    //
    var UserModel = Backbone.Model.extend({
        defaults: {
            userName: null,
            userAvatar: null,
            userTasks: null
        },
        urlRoot: 'user/rest',

        initialize: function() {
            this.currentUserTaskIndex = null;
            window.location.hash = '';
            this.loadUserData();
        },

        loadUserData: function() {
            Util.startLoadingAnimation();
            this.fetch({
                success: function(model, response, options) {
                    Util.stopLoadingAnimation();
                    main.router.navigate("#!app", {trigger: true});
                }
            });
        }
    });

    main.userModel = new UserModel();


    //
    var UserView = Backbone.View.extend({
        el: $('#user'),
        events: {
            'click #userName li>a': function(ev) {
                var href = $(ev.currentTarget).attr('href'),
                    action = href.slice(href.indexOf(':') + 1);
                this[action + 'Action'](ev);
            }
        },

        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'request', Util.startLoadingAnimation);
            this.listenTo(this.model, 'error', function(model, xhr, options) {
                main.syncError(model, xhr, options);
            });
        },

        render: function() {
            this.$el.html(templates['userTemplate'](this.model.attributes));
            return this;
        },

        //
        addUserTaskAction: function(event) {
            var _this = this;
            main.showUserTaskDialog(event.target,
                $(templates['addUserTaskTemplate']()),
                this.model.get('userName') + ': Добавить задачу',
                function() {
                    _this.addUserTask();
                }
            );
        },

        //
        addUserTask: function() {
            var _this = this,
                userTask = $('#formAddTask').find('textarea').val() || null;
            if(userTask) {
                var tasks = this.model.get('userTasks');
                tasks.push(userTask);
                _this.saveUserModelTasks(tasks);
            }
            else {
                $('#dialogMsg').html('Введите текст задачи');
            }
        },

        //
        changeUserTask: function($taskElement) {
            var _this = this,
                selectedUserTaskId = $taskElement.attr('data-id'),
                selectedUserTaskIndex = selectedUserTaskId.slice(selectedUserTaskId.indexOf('_') + 1) || null,
                selectedUserTask = this.model.get('userTasks')[selectedUserTaskIndex];
            this.model.currentUserTaskIndex = selectedUserTaskIndex;
            main.showUserTaskDialog($taskElement,
                $(templates['addUserTaskTemplate']({taskText: selectedUserTask})),
                this.model.get('userName') + ': Редактировать задачу',
                function() {
                    _this.model.get('userTasks').splice(selectedUserTaskIndex, 1);
                    _this.addUserTask();
                });
        },

        //
        deleteUserTask: function() {
            var _this = this,
                selectedUserTaskIndex = this.model.currentUserTaskIndex;
            var tasks = this.model.get('userTasks');
            tasks.splice(selectedUserTaskIndex, 1);
            _this.saveUserModelTasks(tasks);
        },

        //
        saveUserModelTasks: function(tasks) {
            var _this = this;
            this.model.save({userTasks: tasks}, {
                patch: true,
                success: function() {
                    Util.stopLoadingAnimation();
                    _this.model.loadUserData();
                    if(Backbone.history.fragment === '!app') {
                        App.taskCalendarView.render();
                    }
                }
            });
            Main.dialogView.dialogDestroy();
            this.model.currentUserTaskIndex = null;
        },

        //
        saveUserData: function() {
            var _this = this,
                formData = {
                    oldPassword: $('#oldPassword').val(),
                    inputPassword: $('#inputPassword').val(),
                    userDataImg: $('#userDataImg').siblings('span.loadedImg').css('background-image')
                        .replace('url(', '').replace(')', '').replace(/^"(.*)"$/, '$1')
                };
            if(formData['oldPassword']) {
                this.model.save(formData, {
                    success: function() {
                        Util.stopLoadingAnimation();
                        Main.dialogView.dialogDestroy();
                        _this.model.loadUserData();
                    }
                });
            }
            else {
                $('#dialogMsg').html('Введите текущий пароль');
            }
        },

        //
        logoutAction: function() {
            window.location.replace("main/logout");
        },

        //
        userSettingsAction: function(event) {
            main.showUserTaskDialog(event.target,
                $(templates['userDataTemplate'](this.model.attributes)),
                this.model.get('userName') + ': Редактировать профиль',
                function() {
                    main.userView.saveUserData();
                });
        }

    });
    main.userView = new UserView({model: main.userModel});


    //
    var MainModel = Backbone.Model.extend({
        defaults: {},

        initialize: function() {
        }
    });
    var mModel = new MainModel();


    //
    var MainView = Backbone.View.extend({
        el: $('body'),
        events: {
            'click .dropdown, .dropup': 'clickDropdown',
            'mouseleave .dropdown, .dropup': 'leaveDropdown',
            'mouseover .dropdown, .dropup': 'overDropdown',
            'click .accordionSection': 'clickAccordion'
        },

        initialize: function() {
            this.ddOpened = false;
        },

        //
        clickDropdown: function(event) {  //dropdown menus
            event.preventDefault();
            if($(event.currentTarget).find('ul.dropdownMenu').css('display') === 'none') {
                $(event.currentTarget).addClass('open');
                this.ddOpened = true;
            }
            else {
                $(event.currentTarget).removeClass('open');
                this.ddOpened = false;
            }
        },

        //
        leaveDropdown: function() {     //disable dropdown menus
            var $ddMenus = $('.dropdown, .dropup'),
                _this = this;
            $ddMenus.removeClass('open');
            window.setTimeout(function() {
                if(!$ddMenus.hasClass('open')) {
                    _this.ddOpened = false;
                }
            }, 300);
        },

        //
        overDropdown: function(event) { //open next dropdown without click
            if(this.ddOpened) {
                $(event.currentTarget).addClass('open');
            }
        },

        //
        clickAccordion: function(event) {    //accordion
            var $target = $(event.currentTarget);
            if(!$target.is('[href^=#!]')) {
                $target.find('.current').removeClass('current');
                if($target.hasClass('accordionToggler')) {
                    var $accordion = $target.closest('.accordion');
                    $accordion.addClass('current');
                    $target.find('.accordion').not('.collapsed').not('.current').addClass('closed');
                    $accordion.toggleClass('closed');
                }
            }
        },

        //mark current menu
        highlightHref: function() {
            var href = Backbone.history.location.hash,
                $appMenu = $('#appMenu');
            $appMenu.find('li').removeClass('selectedMenu');
            $appMenu.find('a[href$="' + href + '"]').closest('li').addClass('selectedMenu');
        }
    });
    var mView = new MainView();


    //
    var MainRouter = Backbone.Router.extend({
        routes: {
            "!:pageName": "pageRoute"
        },

        pageRoute: function(pageName) {
            var _this = this;
            Util.loadData('main/loadPage', {pageName: pageName})
                .done(function(msg) {
                    _this.closeViews();
                    mView.highlightHref();
                    $('#appContainer').html(msg);
                })
                .fail(function(msg) {
                });
        },

        //close backbone Views
        closeViews: function() {
            if(typeof App !== 'undefined') {
                Object.keys(App).forEach(function(elem) {
                    if(elem.indexOf('View') !== -1) {
                        if(App[elem].collection) {
                            _.each(App[elem].collection.models, function(elem) {
                                elem.viewLink.closeView();
                                elem.off();
                            });
                        }
                        App[elem].closeView();
                    }
                });
            }
        }
    });

    main.router = new MainRouter();
    Backbone.history.start();


    //
    var DialogView = Backbone.View.extend({
        el: $('#dialogContainer'),
        events: {
            'change #projectImg': function(event) {
                Util.loadImg(event, 120);
            },
            'change #userDataImg': function(event) {
                Util.loadImg(event, 36);
            },
            'focus input, textarea': function() {
                $('#dialogMsg').empty();
            },
            'click #dialogCloser': 'dialogDestroy'
        },

        initialize: function() {
            this.beforeClose = null;
        },

        //
        makeDialog: function(params) {
            $('#shadowWrapper').addClass('fog');
            $('#dialogContainer').addClass('scene');
            $('#dialogTitle').html(params.title);
            $('#dialogContent').html(params.$content);
            $.each(params.buttons, function(i, item) {
                $('<button>', {
                    class: 'btn',
                    type: 'button',
                    html: i,
                    click: item
                })
                    .appendTo('#dialogButtons');
            });
            this.beforeClose = params.beforeClose || null;
            params.$content.find('.datepickerLink').pickadate();
            var coords = this.getCoords(params.ev);
            this.$el.css({top: coords.top, left: coords.left});
        },

        //
        dialogDestroy: function() {
            if(typeof this.beforeClose === 'function') {
                this.beforeClose();
                this.beforeClose = null;
            }
            $('#dialogContainer').removeClass('scene');
            $('#dialogTitle').empty();
            $('#dialogContent').empty();
            $('#dialogMsg').empty();
            $('#dialogButtons').find('button').remove();
            $('#shadowWrapper').removeClass('fog');
        },

        //
        getCoords: function(elem) {
            if(elem instanceof $) {
                elem = elem[0];
            }
            var gapY = 4, //px
                gapX = 4;

            var box = elem.getBoundingClientRect(),
                scrollTop = window.pageYOffset,
                scrollLeft = window.pageXOffset,
                elemHeight = $(elem).outerHeight(true),
                elemWidth = $(elem).outerWidth(true),
                dialogHeight = this.$el.outerHeight(true),
                dialogWidth = this.$el.outerWidth(true),
                windowHeight = $(window).height();

            var top = box.top + scrollTop + elemHeight,
                left = box.left + scrollLeft - dialogWidth - gapX;

            //correct editProject position
            if(elem.className === 'showEditProject') {
                if($(elem).closest('.projectThumb').hasClass('arched')) {
                    top -= elemHeight;
                }
                else {
                    top -= 94;
                }
            }
            //correct addProjectTask position
            else if(elem.className === 'showAddTask') {
                top -= 62;
            }
            //correct userSettings position
            else if(elem.tagName === 'A' && elem.getAttribute('href').indexOf('!user') !== -1) {
                left += box.width;
                top = 45;
            }
            //correct editTask position
            else if($(elem).hasClass('selectedTask') || elem.id === 'addProject') {
                left = left + elemWidth + gapX;
                top += gapY;
            }

            //check that places in window at bottom
            if(top + dialogHeight > scrollTop + windowHeight) {
                top = scrollTop + windowHeight - dialogHeight - gapY;
                if($(elem).hasClass('selectedTask')) {
                    left -= dialogWidth;
                }
            }
            //
            if(top < gapY) {
                top = gapY;
            }
            if(left < gapX) {
                left = gapX;
            }

            return {top: Math.round(top), left: Math.round(left)};
        }

    });
    main.dialogView = new DialogView();


    //
    main.showUserTaskDialog = function(ev, $content, title, callback) {
        var butt = {
            'Сохранить': callback
        };
        if(title.indexOf('Редактировать задачу') !== -1) {
            butt['Удалить задачу'] = function() {
                main.userView.deleteUserTask();
            }
        }
        Main.dialogView.makeDialog({
            ev: ev,
            $content: $content,
            title: title,
            buttons: butt
        });
    };


    //
    var AppMsgView = Backbone.View.extend({
        el: $('#appMessage'),
        events: {
            'click #appMessageCloser': 'hideAppMessage'
        },

        //
        showAppMsg: function(msg) {
            this.$el.css({top: '-88', height: '0'});
            $('#shadowWrapper').addClass('fog');
            $('#appMessageText').html(msg);
            this.$el.css({top: $(window).scrollTop() + 70, height: 'auto'});
        },

        //
        hideAppMessage: function() {
            this.$el.css({top: -this.$el.outerHeight(true) - 42});
            $('#shadowWrapper').removeClass('fog');
        }
    });
    main.appMsgView = new AppMsgView();


    //
    main.syncError = function(model, xhr, options) {
        if($('#dialogContainer').hasClass('scene')) {
            $('#dialogMsg').html(xhr.responseText);
        }
        else {
            main.appMsgView.showAppMsg(xhr.responseText);
        }
        Util.stopLoadingAnimation();
    };


    //
    window.onerror = function(message, source, line) {
        Main.appMsgView.showAppMsg('JS error: ' + message + '<br/>src: ' + source + '<br/>line: ' + line);
    };


    return main;
})(window, jQuery, Backbone, _);