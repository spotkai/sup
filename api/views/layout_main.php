<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Производство</title>
    <link rel="icon" type="image/ico" href="img/favicon.ico"/>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="css/picker-classic.css"/>
    <link rel="stylesheet" type="text/css" href="css/picker-classic-date.css"/>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
</head>

<body>
<div id="shadowWrapper"></div>
<div id="ajaxShadow"></div>

<div id="mainWrapper">
    <div id="appMessageWrap">
        <div id="appMessage">
            <div id="appMessageText"></div>
            <div id="appMessageCloser"></div>
        </div>
    </div>

    <div id="dialogContainer">
        <div id="dialogTitle"></div>
        <div id="dialogContent"></div>
        <div id="dialogMsg"></div>
        <div id="dialogButtons"></div>
        <div id="dialogCloser"></div>
    </div>

    <header id="mainHeader">
        <div id="appMenu">
            <ul>
                <li><a href="#!app">Проекты</a></li>
                <li><a href="#!prod">Производство</a></li>
                <li><a href="#!docs">Документы</a></li>
                <li><a href="#!conts">Контакты</a></li>
            </ul>
        </div>

        <div id="ajaxLoader"></div>
        <div id="user"></div>
    </header>

    <div id="appContainer"></div>
</div>

<script src="js/lib/jquery-2.1.3.min.js"></script>
<script src="js/lib/underscore-min.js"></script>
<script src="js/lib/backbone-min.js"></script>
<script src="js/lib/picker.min.js"></script>
<script src="js/lib/picker-date.min.js"></script>
<script src="js/monitor.js"></script>
<script src="js/util.js"></script>
<script src="js/templates.js"></script>
<script src="js/main.js"></script>
</body>
</html>