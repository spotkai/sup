<header id="header">
    <div id="projectMenu">
        <ul>
            <li id="selectProjects" class="dropdown">
                <a href="#statusTab">Заказы</a>
                <span class="statusSelected">~рабочие~</span>
                <ul class="dropdownMenu">
                    <li><span class="status-work"></span><a href="#!status:work">Рабочие</a></li>
                    <li><span class="status-query"></span><a href="#!status:query">Запросы</a></li>
                    <li><span class="status-arch"></span><a href="#!status:arch">Архив</a></li>
                </ul>
            </li>
            <li id="addProject">Добавить<br/>проект</li>
            <li id="reloadPage">Обновить<br/>проекты</li>
            <li id="projectCalendar">Календарь</li>
        </ul>
    </div>

    <div class="searchForm">
        <input type="search" placeholder="найти проект">
        <span class="searchIcon"></span>
    </div>
</header>

<div id="main">
    <div id="taskCalendar">
        <div id="taskCalendarContent"></div>
    </div>

    <div id="projectList">
        <div id="projectListContent"></div>

        <div id="toggleStatePointTool" class="toolElement">
            <ul>
                <li data-id="inwork">в работе</li>
                <li data-id="finished">завершено</li>
                <li data-id="noneed">не нужно</li>
            </ul>
        </div>
        <div id="pickadateContainer"></div>
    </div>
</div>

<script src="js/app.js"></script>