<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ошибка</title>
    <link rel="icon" type="image/ico" href="../img/favicon.ico"/>
    <link rel="shortcut icon" href="../img/favicon.ico"/>
    <link rel="stylesheet" href="../css/reset.css"/>
    <link rel="stylesheet" href="../css/error.css"/>
</head>

<body>
<div id="errorWrap">
    <div id="errorTitle">
        <h2><?php echo($errorCode); ?>#</h2>
    </div>

    <div id="errorMsg">
        <h2><?php echo($errorMsg); ?></h2>
    </div>

    <div id="errorFooter">
        <a href="../user">войти в систему</a>
    </div>
</div>
</body>
</html>