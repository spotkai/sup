<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Вход в систему</title>
    <link rel="icon" type="image/ico" href="img/favicon.ico"/>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="css/picker-classic.css"/>
    <link rel="stylesheet" type="text/css" href="css/picker-classic-date.css"/>
    <link rel="stylesheet" type="text/css" href="css/login.css"/>
</head>

<body>
<div id="loginWrap">
    <div id="formLoginWrap">
        <form id="formLogin" method="POST" action="#">
            <div id="loginTitle">
                <h2>Вход в систему</h2>
                <div id="ajaxLoader"></div>
            </div>

            <div class="formItem">
                <label for="loginName">Логин</label>
                <input type="text" id="loginName" name="loginName" required/>
                <span></span>
            </div>

            <div class="formItem">
                <label for="loginPassword">Пароль</label>
                <input type="password" id="loginPassword" name="loginPassword" required/>
            </div>

            <div id="loginMsg"></div>

            <div class="formAction">
                <button class="btn" type="button" name="enterLogin">Войти</button>
            </div>

            <a id="openReg" href="#">регистрация</a>
        </form>
    </div>

    <div id="formRegWrap">
        <form id="formReg" method="POST" action="#" autocomplete="off">
            <h3>Создать пользователя</h3>

            <div class="formItem">
                <label for="regName">Логин</label>
                <input type="text" id="regName" name="regName" required/>
                <span></span>
            </div>

            <div class="formItem">
                <label for="regFirstname">Имя</label>
                <input type="text" id="regFirstname" name="regFirstname" required/>
                <span></span>
            </div>

            <div class="formItem">
                <label for="regLastname">Фамилия</label>
                <input type="text" id="regLastname" name="regLastname" required/>
                <span></span>
            </div>

            <div class="formItem">
                <label for="regPassword">Пароль</label>
                <input type="password" id="regPassword" name="regPassword" required/>
            </div>

            <div class="formItem">
                <label for="regPassword2">Повтор пароля</label>
                <input type="password" id="regPassword2" name="regPassword2" required/>
            </div>

            <div class="formItem">
                <label for="userRegImg">Аватар</label>
                <input id="userRegImg" type="file" name="userRegImg"/>
                <span class="loadedImg"
                      style="background-image: url(data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)">
                </span>
            </div>

            <div class="formItem">
                <label for="regKey">Ключ</label>
                <input type="text" id="regKey" name="regKey" required/>
            </div>

            <div id="regMsg"></div>

            <div class="formAction">
                <button class="btn" type="button" name="enterReg">Создать</button>
            </div>
        </form>
    </div>
</div>

<script src="js/lib/jquery-2.1.3.min.js"></script>
<script src="js/lib/underscore-min.js"></script>
<script src="js/lib/backbone-min.js"></script>
<script src="js/lib/picker.min.js"></script>
<script src="js/lib/picker-date.min.js"></script>
<script src="js/util.js"></script>
<script src="js/user.js"></script>
</body>
</html>