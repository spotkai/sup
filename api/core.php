<?php
//todo move it to main.php::start()

define('ROOT_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/sup'); //fixme 'sup' for localhost debug only

header('Content-Type: text/html; charset=utf-8');
header('X-Content-Type-Options: nosniff'); //fixme must be in httpd-config ??
header('X-XSS-Protection: 1; mode=block'); //fixme must be in httpd-config ??
header('X-Frame-Options: deny'); //fixme must be in httpd-config ??
//header('Strict-Transport-Security: max-age=31536000; includeSubdomains'); //fixme add for HTTPS

//default controller & action
$controllerName = 'user';
$actionName = 'default';

$routes = $_SERVER['REQUEST_URI'];
if(($pos = strrpos($routes, '?')) !== false) {
    $routes = substr($routes, 0, $pos);
}
$routes = explode('/', $routes);
array_shift($routes); //todo remove! it's for localhost debug only

if(!empty($routes[1])) {
    $controllerName = $routes[1];
}
if(!empty($routes[2]) && $controllerName !== 'error') {
    $actionName = $routes[2];
}

$controllerName = 'Controller_' . strtolower($controllerName);
$actionName = 'action_' . strtolower($actionName);
$controllerPath = 'controllers/' . $controllerName . '.php';
if(file_exists($controllerPath)) {
    include $controllerPath;
}
else {
    error('404#');
}

$controller = new $controllerName;
if(method_exists($controller, $actionName)) {
    $controller->$actionName();
}
else {
    error('405#');
}


//
function error($errorCode) {
    header('Location: ' . ROOT_PATH . '/error/' . $errorCode);
    exit();
}