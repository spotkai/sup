<?php
require 'Api.php';

class Controller_main extends Api {

    public $view = 'views/layout_error.php';    //fixme set default - error view

    public function __construct() {
        parent::__construct();
    }


    //
    function action_default() {
        if(!$this->enterAuth()) {
            header('Location: ' . ROOT_PATH . '/error/401#');
            exit();
        }
        else {
            include('views/layout_main.php');
        }
    }


    //
    function enterAuth() {
        if(isset($_COOKIE['MPID']) && isset($_COOKIE['MPHASH'])) {
            $userName = $_COOKIE['MPID'];
            $this->dbConnect();
            $sql = 'SELECT userPassword, userCode, userHash FROM users WHERE userName=? LIMIT 1';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $userName);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows === 1) {
                $stmt->bind_result($userPassword, $userCode, $userHash);
                $stmt->fetch();
                if($userHash === $_COOKIE['MPHASH'] && $userHash === $this->getUserHash($userPassword, $userCode)) {
                    date_default_timezone_set("Europe/Minsk");
                    $now = date('Y-m-d H:i:s');
                    $sql = 'UPDATE users SET userLastVisit=? WHERE userName=?';
                    $stmt->free_result();
                    $stmt = $this->connection->prepare($sql);
                    $stmt->bind_param('ss', $now, $userName);
                    $stmt->execute();

                    $stmt->close();
                    $this->dbDisconnect();
                    return true;
                }
            }

            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();
        }

        $this->clearIdentity();
        return false;
    }


    //
    function action_logout() {
        $this->clearIdentity();
        header('Location: ' . ROOT_PATH . '/user');
        exit();
    }


    //
    function action_checkLock() {    //fixme ??remove to controller_app.php
        date_default_timezone_set("Europe/Minsk");
        $projectId = +$_POST['projectId'];
        $userId = +$_POST['userId'];
        $this->dbConnect();
        $sql = 'SELECT userName, lockTime FROM locked WHERE projectId=? LIMIT 1';
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('i', $projectId);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->num_rows === 1) { //locked
            $stmt->bind_result($userName, $lockTime);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();

            $timeLimit = 15; //min
            $timePassed = (time() - strtotime($lockTime)) / 60;
            if(+$userName === $userId) {
                $res = 'OK';  //same user
            }
            else if($timePassed > $timeLimit) {   //time elapsed
                $this->action_freeLock();
                $this->lockProject();    //lock for new user
                $res = 'OK';
            }
            else {
                $res = $userName;
            }
        }
        else {
            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();
            $this->lockProject();
            $res = 'OK';
        }

        if($res === 'OK') {
            $res = $this->loadProject($projectId);
        }

        echo $res;
    }


    //
    function loadProject($projectId) {
        $this->dbConnect();
        $sql = "SELECT projectName, projectState, projectImg, projectParams, projectTasks, projectData, projectDateFinish
                    FROM projects WHERE projectId=?";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param("i", $projectId);
        $stmt->execute();
        $stmt->bind_result($projectName, $projectState, $projectImg, $projectParams, $projectTasks, $projectData, $projectDateFinish);
        $stmt->fetch();
        $stmt->close();
        $this->dbDisconnect();

        $data['id'] = $projectId;
        $data['projectName'] = $projectName;
        $data['projectState'] = $projectState;
        $data['projectImg'] = $projectImg;
        $data['projectParams'] = json_decode($projectParams, true);
        $data['projectTasks'] = json_decode($projectTasks, true);
        $data['projectData'] = json_decode($projectData, true);
        $data['projectDateFinish'] = $projectDateFinish;

        header('Content-Type: application/json; charset=utf-8');
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    //
    function action_freeLock() {
        $projectId = +$_POST['projectId'];
        $this->dbConnect();
        $sql = "DELETE FROM locked WHERE projectId=?";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param("i", $projectId);
        $stmt->execute();
        $stmt->close();
        $this->dbDisconnect();
    }


    //
    function lockProject() {
        $projectId = +$_POST['projectId'];
        $userId = +$_POST['userId'];
        $now = date('Y-m-d H:i:s');
        $this->dbConnect();
        $sql = "INSERT INTO locked (projectId, userName, lockTime) VALUES (?, ?, ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('iis', $projectId, $userId, $now);
        $stmt->execute();
        $stmt->close();
        $this->dbDisconnect();
    }


    //
    function action_loadPage() {
        $this->checkAccess();
        $page = $_POST['pageName'];
        $view = 'views/view_' . $page . '.php';
        if(file_exists($view)) {
            include $view;
        }
        else {
            exit('<script>window.location.replace("error/404#")</script>');
        }
    }

}