<?php
require 'Api.php';

class Controller_app extends Api {

    public function __construct() {
        parent::__construct();
    }


    //
    function action_rest() { //todo make return sql errors (i.e. if no rows affected) (i.e. add proj with same name)
        $this->checkAccess();
        $data = []; //todo +show errors mechanism
        switch($this->method) {
            case 'POST':
                $this->addProject();
                break;
            case 'GET':
                $data = $this->getProjects();
                break;
            case 'PUT':
                break;
            case 'PATCH':
                $this->updateProjectInfo();
                break;
            case 'DELETE':
                //                $this->deleteProject();
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
        $this->_response($data);
    }


    //
    public function getProjects() {
        if(!isset($_GET['state'])) {
            exit();
        }

        $state = preg_replace('/[^A-Za-z0-9_\-]/', '', $_GET['state']);
        $this->dbConnect();
        $sql = "SELECT projectId, projectName, projectImg, projectParams, projectTasks, projectData, projectDateStart, projectDateFinish
            FROM projects WHERE projectState=?";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param("s", $state);
        $stmt->execute();
        $stmt->store_result();

        $data = [];
        if($stmt->num_rows > 0) {
            $clients = json_decode(file_get_contents('mp_customers.json'), true);
            $stmt->bind_result($projectId, $projectName, $projectImg, $projectParams, $projectTasks, $projectData,
                $projectDateStart, $projectDateFinish);
            $i = 0;
            while($stmt->fetch()) {
                $data[$i]['id'] = $projectId;
                $data[$i]['projectName'] = $projectName;
                $data[$i]['projectState'] = $state;
                $data[$i]['projectImg'] = $projectImg;
                $data[$i]['projectParams'] = json_decode($projectParams, true);
                $data[$i]['projectTasks'] = json_decode($projectTasks, true);
                $data[$i]['projectData'] = json_decode($projectData, true);
                $data[$i]['projectDateStart'] = $projectDateStart;
                $data[$i]['projectDateFinish'] = $projectDateFinish;

                $projectCode = mb_substr($projectName, 8, null, 'UTF-8');
                @$data[$i]['projectClient'] = mb_substr($clients[$projectCode], 0, 48, 'UTF-8') ?: '';
                $i++;
            }
        }

        $stmt->free_result();
        $stmt->close();
        $this->dbDisconnect();
        return $data;
    }


    //
    public function addProject() {
        date_default_timezone_set("Europe/Minsk");
        $dataTemplate = [
            [
                ['groupName' => 'Проектирование'],
                ['title' => 'согласование: 3d / эскизы', 'status' => 'inwork'],
                ['title' => 'согласование: тех. задание', 'status' => 'inwork', 'date' => null],
                ['title' => '3d-проектирование', 'status' => 'inwork'],
                ['title' => 'чертежи: листовые', 'status' => 'inwork'],
                ['title' => 'чертежи: ДСП', 'status' => 'inwork'],
                ['title' => 'чертежи: точеные', 'status' => 'inwork'],
                ['title' => 'чертежи: из трубы', 'status' => 'inwork']
            ],
            [
                ['groupName' => 'Производство'],
                ['title' => 'заказ: листовые', 'status' => 'inwork', 'date' => null],
                ['title' => 'заказ: раскрой ДСП / фурнитура', 'status' => 'inwork', 'date' => null],
                ['title' => 'заказ: комплектующие', 'status' => 'inwork'],
                ['title' => 'заказ: точеные', 'status' => 'inwork', 'date' => null],
                ['title' => 'детали: из листа', 'status' => 'inwork'],
                ['title' => 'детали: доски ДСП', 'status' => 'inwork'],
                ['title' => 'детали: точеные', 'status' => 'inwork'],
                ['title' => 'детали: из трубы', 'status' => 'inwork'],
                ['title' => 'сварка', 'status' => 'inwork'],
                ['title' => 'покраска', 'status' => 'inwork']
            ],
            [
                ['groupName' => 'Отгрузка'],
                ['title' => 'док: спецификация изделия', 'status' => 'inwork'],
                ['title' => 'док: разметка ДСП', 'status' => 'inwork'],
                ['title' => 'док: инструкции / паспорта', 'status' => 'inwork'],
                ['title' => 'док: упак. документация', 'status' => 'inwork'],
                ['title' => 'комплектование', 'status' => 'inwork'],
                ['title' => 'сборка / упаковка', 'status' => 'inwork'],
                ['title' => 'отгрузка', 'status' => 'inwork', 'date' => null],
                ['title' => 'монтаж', 'status' => 'inwork'],
                ['title' => 'оплата заказа', 'status' => 'inwork', 'date' => null]
            ]
        ];
        $tasksTemplate = [];

        $data = json_decode(file_get_contents('php://input'), true);
        $projectName = $data['projectName'];
        $projectState = $data['projectState'];
        $projectImg = $data['projectImg'];
        $projectParams = json_encode($data['projectParams'], JSON_UNESCAPED_UNICODE);
        $projectTasks = json_encode($tasksTemplate, JSON_UNESCAPED_UNICODE);
        $projectData = json_encode($dataTemplate, JSON_UNESCAPED_UNICODE);
        $projectDateStart = date('Y-m-d H:i:s'); //now
        $projectDateFinish = $data['projectDateFinish'];

        $this->dbConnect();
        $sql = 'SELECT projectId FROM projects WHERE projectName=?';
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('s', $projectName);
        $stmt->execute();
        $stmt->bind_result($projectId);
        $stmt->fetch();
        $stmt->close();
        if($projectId) {
            $this->dbDisconnect();
            exit('Проект "' . $projectName . '" уже существует.');
        }

        $sql = "INSERT INTO projects (projectName, projectState, projectImg, projectParams, projectTasks, projectData,
                  projectDateStart, projectDateFinish) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param("ssssssss", $projectName, $projectState, $projectImg, $projectParams, $projectTasks, $projectData,
            $projectDateStart, $projectDateFinish);
        $stmt->execute();
        $stmt->close();
        $this->dbDisconnect();
    }


    //
    function updateProjectInfo() {
        $data = json_decode(file_get_contents('php://input'), true);
        $projectName = @$data['projectName'];
        $projectState = @$data['projectState'];
        $projectTasks = json_encode(@$data['projectTasks'], JSON_UNESCAPED_UNICODE);
        $projectData = json_encode(@$data['projectData'], JSON_UNESCAPED_UNICODE);
        $projectId = $this->id;

        $this->dbConnect();
        if($projectName) {
            $projectState = $data['projectState'];
            $projectImg = $data['projectImg'];
            $projectParams = json_encode($data['projectParams'], JSON_UNESCAPED_UNICODE);
            $projectDateFinish = $data['projectDateFinish'];
            $sql = "UPDATE projects SET projectName=?, projectState=?, projectImg=?, projectParams=?, projectDateFinish=? WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("sssssi", $projectName, $projectState, $projectImg, $projectParams, $projectDateFinish, $projectId);
        }
        elseif($projectState) {
            $sql = "UPDATE projects SET projectState=? WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $projectState, $projectId);
        }
        elseif($projectTasks && $projectTasks !== 'null') {
            $sql = "UPDATE projects SET projectTasks=? WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $projectTasks, $projectId);
        }
        elseif($projectData && $projectData !== 'null') {
            $sql = "UPDATE projects SET projectData=? WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $projectData, $projectId);
        }

        if(isset($stmt)) {
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        else {
            $this->dbDisconnect();
            http_response_code(500);
            exit('Ошибка передачи данных');
        }
    }


    //
    //    public function deleteProject() {
    //        $projectId = $this->id;
    //
    //        $this->dbConnect();
    //        $sql = "DELETE FROM projects WHERE projectId=?";
    //        $stmt = $this->connection->prepare($sql);
    //        $stmt->bind_param("i", $projectId);
    //        $stmt->execute();
    //        $stmt->close();
    //        $this->dbDisconnect();
    //    }

}