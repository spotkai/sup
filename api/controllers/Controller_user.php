<?php
require 'Api.php';    //fixme move all, except rest, to main.php

class Controller_user extends Api {

    function __construct() {
        parent::__construct();
    }


    //
    function action_default() {
        if(isset($_COOKIE['MPID']) && isset($_COOKIE['MPHASH'])) {
            header('Location: ' . ROOT_PATH . '/main');
            exit();
        }
        else {
            include('views/layout_user.php');
        }
    }


    //
    function action_rest() {
        $this->checkAccess();
        $data = [];
        switch($this->method) {
            case 'GET':
                $data = $this->getUserData();
                break;
            case 'PUT':
                $this->updateUserData();
                break;
            case 'PATCH':
                $this->updateUserTasks();
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
        $this->_response($data);
    }


    //
    function action_loginUser() {
        $loginName = preg_replace('/[^A-Za-z0-9_\-]/', '', $_POST['loginName']);
        $loginPassword = filter_input(INPUT_POST, 'loginPassword', FILTER_SANITIZE_STRING);
        if("" === $loginName || "" === $loginPassword) {
            exit("Не указан логин/пароль");
        }

        $this->dbConnect();
        $sql = 'SELECT userPassword, userCode, userLastVisit, userTryCount, userHash FROM users WHERE userName=? LIMIT 1';
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('s', $loginName);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->num_rows === 1) { //login true
            date_default_timezone_set("Europe/Minsk");
            $stmt->bind_result($userPassword, $userCode, $userLastVisit, $userTryCount, $userHash);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();

            $tryLimit = 5;
            if($userTryCount > 5) {
                $timeLimit = 1; //min
            }
            else {
                $timeLimit = 5; //min
            }
            $timeLeft = $timeLimit - (time() - strtotime($userLastVisit)) / 60;
            if($userTryCount >= $tryLimit && $timeLeft > 0) {
                $this->dbDisconnect();
                exit($userTryCount . ' неправильных попыток.</br>Вход блокирован на ' . ceil($timeLeft) . ' мин.');
            }

            $now = date('Y-m-d H:i:s');
            $sql = 'UPDATE users SET userLastVisit=?, userTryCount=?, userHash=? WHERE userName=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('siss', $now, $userTryCount, $userHash, $loginName);
            if(crypt($loginPassword, $userCode) === $userPassword) { //password true
                $userTryCount = 0;
                $userHash = $this->setLoginCookies($loginName, $userPassword, $userCode);
                $stmt->execute();
                $stmt->close();
                $this->dbDisconnect();
                exit('<script>window.location.replace("main")</script>');
            }

            else {
                $userTryCount += 1;
                $userHash = null;
                $stmt->execute();
                $stmt->close();
                $this->dbDisconnect();
                exit('Неверный пароль');
            }
        }
        else {
            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();
            exit('Неверный логин');
        }
    }


    //
    function action_checkUserName() {
        $userName = $_POST['loginName'];
        $sql = 'SELECT userAvatar FROM users WHERE userName=? LIMIT 1';
        $this->dbConnect();
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('s', $userName);
        $stmt->execute();
        $stmt->bind_result($userAvatar);
        $stmt->fetch();
        $stmt->close();
        $this->dbDisconnect();
        if($userAvatar) {
            echo $userAvatar;
        }
        else {
            echo 'ERROR';
        }
    }


    //
    function getUserData() {
        $userName = $_COOKIE['MPID'];
        $sql = 'SELECT userId, userAvatar, userTasks FROM users WHERE userName=? LIMIT 1';
        $this->dbConnect();
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('s', $userName);
        $stmt->execute();
        $stmt->bind_result($userId, $userAvatar, $userTasks);
        $stmt->fetch();
        $stmt->close();

        $sql = 'SELECT userId, userFirstname, userLastname FROM users';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($byUserId, $userFirstname, $userLastname);
        $allUsers = [];
        while($stmt->fetch()) {
            $allUsers[$byUserId] = $userFirstname . ' ' . $userLastname;
        }

        $this->dbDisconnect();
        if($userAvatar) {
            return [
                'id' => $userId,
                'userName' => $userName,
                'userAvatar' => $userAvatar,
                'userTasks' => json_decode($userTasks, true),
                'allUsers' => $allUsers
            ];
        }
        else {
            return 'Неверный логин2';
        }
    }


    //
    function updateUserTasks() {
        $data = json_decode(file_get_contents('php://input'), true);
        $userTasks = json_encode($data['userTasks'], JSON_UNESCAPED_UNICODE);
        $userId = $this->id;

        $this->dbConnect();
        $sql = "UPDATE users SET userTasks=? WHERE userId=?";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param("si", $userTasks, $userId);
        $stmt->execute();
        $stmt->close();
        $this->dbDisconnect();
    }


    //
    function updateUserData() {
        $data = json_decode(file_get_contents('php://input'), true);
        $userName = $data['userName'];
        $oldPassword = $data['oldPassword'];
        $inputPassword = $data['inputPassword'];
        $userAvatar = $data['userDataImg'];
        $userId = $this->id;

        if(!$oldPassword) {
            exit("Не указан текущий пароль");
        }
        else {
            $this->dbConnect();
            $sql = 'SELECT userPassword, userCode FROM users WHERE userId=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('i', $userId);
            $stmt->execute();
            $stmt->bind_result($userPassword, $userCode);
            $stmt->fetch();
            $stmt->close();
            if(crypt($oldPassword, $userCode) !== $userPassword) {
                $this->dbDisconnect();
                exit('Неверный текущий пароль');
            }
        }

        if($inputPassword && $userAvatar) {
            if(strlen($inputPassword) < 3) {
                exit("Длина нового пароля менее 3 символов");
            }
            $pass = $this->genPass($inputPassword);
            $newPassword = $pass['userPassword'];
            $newCode = $pass['userCode'];
            $newHash = $this->setLoginCookies($userName, $newPassword, $newCode);

            $sql = "UPDATE users SET userPassword=?, userCode=?, userAvatar=?, userHash=? WHERE userId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("ssssi", $newPassword, $newCode, $userAvatar, $newHash, $userId);
        }
        elseif($userAvatar) {
            $sql = "UPDATE users SET userAvatar=? WHERE userId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $userAvatar, $userId);
        }
        $stmt->execute();
        $stmt->close();
        $this->dbDisconnect();
    }


    //
    function action_registerUser() {
        $key = '1234512345';
        $userName = preg_replace('/[^A-Za-z0-9_\-]/', '', $_POST['regName']);
        $regPassword = filter_input(INPUT_POST, 'regPassword', FILTER_SANITIZE_STRING);
        $regPassword2 = filter_input(INPUT_POST, 'regPassword2', FILTER_SANITIZE_STRING);
        $userAvatar = $_POST['userRegImg'];
        $regFirstname = preg_replace('/[^\p{Cyrillic}A-Za-z0-9_\-]/u', '', $_POST['regFirstname']);
        $regLastname = preg_replace('/[^\p{Cyrillic}A-Za-z0-9_\-]/u', '', $_POST['regLastname']);
        $regKey = preg_replace('/[^A-Za-z0-9_\-]/', '', $_POST['regKey']);

        if("" === $userName || "" === $regPassword || "" === $regPassword2) {
            exit("Не указан логин/пароль");
        }
        if("" === $regFirstname || "" === $regLastname) {
            exit("Не указаны имя/фамилия");
        }
        if($regPassword !== $regPassword2) {
            exit("Пароли не совпадают");
        }
        if(strlen($userName) < 3 || strlen($regPassword) < 3) {
            exit("Длина логина/пароля менее 3 символов");
        }
        if($regKey !== $key) {
            exit("Неверный ключ");
        }
        $this->dbConnect();
        $sql = 'SELECT userId FROM users WHERE userName=?';
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('s', $userName);
        $stmt->execute();
        $stmt->bind_result($userId);
        $stmt->fetch();
        $stmt->close();
        if($userId || $userId > 100) {  //temporary limit
            $this->dbDisconnect();
            exit("Такой пользователь уже существует");
        }

        date_default_timezone_set("Europe/Minsk");
        $now = date('Y-m-d H:i:s');
        $pass = $this->genPass($regPassword);
        $userPassword = $pass['userPassword'];
        $userCode = $pass['userCode'];
        $userHash = $this->setLoginCookies($userName, $userPassword, $userCode);
        $userTasks = json_encode([], JSON_UNESCAPED_UNICODE);

        $sql = "INSERT INTO users (userName, userPassword, userCode, userAvatar, userFirstname, userLastname,
                userLastVisit, userHash, userTasks) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param("sssssssss", $userName, $userPassword, $userCode, $userAvatar, $regFirstname, $regLastname,
            $now, $userHash, $userTasks);
        $stmt->execute();
        $stmt->close();
        $this->dbDisconnect();
        exit('<script>window.location.replace("main")</script>');
    }

}
