<?php

class Api {

    private $username = 'admin';
    private $password = '72943886';
    private $hostname = 'localhost:3306';
    private $defaultDB = 'mp_work';

    protected $connection = false;
    protected $method = '';
    protected $id = null;


    //
    protected function __construct() {
        $this->method = $_SERVER['REQUEST_METHOD'];
        if($this->method === 'PUT' || $this->method === 'PATCH' || $this->method === 'DELETE') {
            $params = explode('/', rtrim($_SERVER['REQUEST_URI'], '/'));
            $this->id = array_pop($params);
        }
    }


    //
    protected function processAPI() {
    }


    //
    protected function _response($data, $status = 200) {    //fixme resp status+msg
        switch($status) {
            case 200:
                $requestStatus = 'OK';
                break;
            case 404:
                $requestStatus = 'Not Found';     //fixme remove
                break;
            case 405:
                $requestStatus = 'Method Not Allowed';  //fixme remove
                break;
            default :
                $status = 500;
                $requestStatus = 'Internal Server Error';  //fixme remove
        }

        header("HTTP/1.1 " . $status . " " . $requestStatus);    //fixme
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    //
    protected function dbConnect() {
        if(!$this->connection) {
            @$this->connection = new mysqli($this->hostname, $this->username, $this->password, $this->defaultDB);
            if(mysqli_connect_errno()) {
                $errorMsg = 'MySQL ERROR: ' . mysqli_connect_error();
                $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
                if($isAjax) {
                    http_response_code(503);
                    exit($errorMsg);
                }
                else {
                    header('Location: ' . ROOT_PATH . '/error/503#');
                    exit();
                }
            }
        }
    }


    //
    protected function dbDisconnect() {
        if($this->connection) {
            $this->connection->close();
            $this->connection = false;
        }
    }


    //
    protected function getUserHash($userPassword, $userCode) {
        $userBrowser = substr($_SERVER['HTTP_USER_AGENT'], -31);
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userIP = substr($userIP, 0, strrpos($userIP, '.'));
        $userCode = substr($userCode, -31);
        $hash = crypt($userIP . $userPassword . $userBrowser, $userCode);  //72 chars input max
        return $hash;
    }


    //
    protected function getUserToken($loginName) {
        $userBrowser = substr($_SERVER['HTTP_USER_AGENT'], -51);
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userIP = substr($userIP, 0, strrpos($userIP, '.'));
        $token = md5($userIP . $userBrowser . $loginName);
        return $token;
    }


    //
    protected function genPass($pass) {
        $userCode = '$2a$10$' . substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()))), 0, 22) . '$';
        $userPassword = crypt($pass, $userCode);
        return ['userCode' => $userCode, 'userPassword' => $userPassword];
    }


    //
    protected function setLoginCookies($loginName, $userPassword, $userCode) {
        $userHash = $this->getUserHash($userPassword, $userCode);
        $token = $this->getUserToken($loginName);
        setcookie("MPID", $loginName, time() + 3600 * 24 * 7, '/', null, null, true); //1 week
        setcookie("MPHASH", $userHash, time() + 3600 * 24 * 7, '/', null, null, true);
        setcookie("MPT", $token, time() + 3600 * 24 * 7, '/', null, null, true);  //fixme set token for 1 hour ??
        return $userHash;
    }


    //
    protected function clearIdentity() {
        if(isset($_COOKIE['MPID'])) {
            $userName = $_COOKIE['MPID'];
            $this->dbConnect();
            $sql = 'UPDATE users SET userHash=NULL WHERE userName=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $userName);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }

        unset($_COOKIE['MPID']);
        unset($_COOKIE['MPHASH']);
        unset($_COOKIE['MPT']);
        setcookie('MPID', '', time() - 3600, '/', null, null, true);
        setcookie('MPHASH', '', time() - 3600, '/', null, null, true);
        setcookie('MPT', '', time() - 3600, '/', null, null, true);
    }


    //
    protected function checkAccess() {
        if(isset($_COOKIE['MPID']) && isset($_COOKIE['MPHASH']) && isset($_COOKIE['MPT'])
            && $this->getUserToken($_COOKIE['MPID']) === $_COOKIE['MPT']
        ) {
            return true;
        }

        $this->clearIdentity();

        $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        if($isAjax) {
            exit('<script>window.location.replace("error/401#")</script>');
        }
        else {
            header('Location: ' . ROOT_PATH . '/error/401#');
            exit();
        }
    }

}
